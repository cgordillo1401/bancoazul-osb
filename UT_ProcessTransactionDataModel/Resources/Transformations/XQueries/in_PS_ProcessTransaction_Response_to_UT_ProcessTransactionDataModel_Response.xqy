xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.bancoazul.com/CO/Operation/ProcessTransaction/V1.0";
(:: import schema at "../../Schemas/ProcessTransactionV1.0.xsd" ::)

declare variable $code as xs:string external;
declare variable $message as xs:string external;

declare function local:func($code as xs:string, 
                            $message as xs:string) 
                            as element() (:: schema-element(ns1:processTransactionResponse) ::) {
    <ns1:processTransactionResponse>
        <ns1:code>{fn:data($code)}</ns1:code>
        <ns1:message>{fn:data($message)}</ns1:message>
    </ns1:processTransactionResponse>
};

local:func($code, $message)