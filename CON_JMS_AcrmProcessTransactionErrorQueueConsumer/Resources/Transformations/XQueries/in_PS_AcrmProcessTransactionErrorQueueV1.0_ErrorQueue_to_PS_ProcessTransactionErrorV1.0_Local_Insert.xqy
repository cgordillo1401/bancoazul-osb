xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/T24ProcessTransactionError_Table";
(:: import schema at "../../../../CON_JCA_AcrmProcessTransactionError/Resources/JCA/T24ProcessTransactionError_Table.xsd" ::)

declare variable $operation as xs:string external;
declare variable $body as xs:string external;
declare variable $code as xs:string external;
declare variable $message as xs:string external;
declare variable $status as xs:string external;

declare function local:func($operation as xs:string, 
                            $body as xs:string, 
                            $code as xs:string, 
                            $message as xs:string, 
                            $status as xs:string) 
                            as element() (:: schema-element(ns1:T24ProcessTransactionError_TableInput) ::) {
    <ns1:T24ProcessTransactionError_TableInput>
        <ns1:operation>{fn:data($operation)}</ns1:operation>
        <ns1:body>{fn:data($body)}</ns1:body>
        <ns1:creationDate>{fn:current-dateTime()}</ns1:creationDate>
        <ns1:code>{fn:data($code)}</ns1:code>
        <ns1:messageResult>{fn:data($message)}</ns1:messageResult>
        <ns1:statusTransaction>{fn:data($status)}</ns1:statusTransaction>
    </ns1:T24ProcessTransactionError_TableInput>
};

local:func($operation, $body, $code, $message, $status)