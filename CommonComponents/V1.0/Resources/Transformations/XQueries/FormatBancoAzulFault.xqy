xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.bancoazul.com/CO/Entities/Commons/BancoAzulFault/V1.0";
(:: import schema at "../../../../../UT_DataModel/V1.0/Entities/CommonBusiness/BancoAzulFaultV1.0.xsd" ::)

declare variable $traceabilityId as xs:string external;
declare variable $categoryCode as xs:string external;
declare variable $categoryDescriptions as xs:string external;
declare variable $location as xs:string external;
declare variable $technicalDescription as element() external;

declare function local:func($traceabilityId as xs:string, 
                            $categoryCode as xs:string, 
                            $categoryDescriptions as xs:string,$location as xs:string ,
                            $technicalDescription as element()) 
                            as element() (:: schema-element(ns1:bancoAzulFault) ::) {
    <ns1:bancoAzulFault>
        <ns1:traceabilityId>{fn:data($traceabilityId)}</ns1:traceabilityId>
        <ns1:categoryCode>{fn:data($categoryCode)}</ns1:categoryCode>
        <ns1:categoryDescription>{fn:data($categoryDescriptions)}</ns1:categoryDescription>
        <ns1:location>{fn:data($location)}</ns1:location>
        <ns1:technicalDescription>{$technicalDescription}</ns1:technicalDescription>
    </ns1:bancoAzulFault>
};

local:func($traceabilityId, $categoryCode, $categoryDescriptions,$location, $technicalDescription)