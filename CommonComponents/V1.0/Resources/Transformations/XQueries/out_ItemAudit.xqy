xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.bancoazul.com/CO/Schema/Operation/SaveAudit/V1.0";
(:: import schema at "../../../../../UT_Audit/V1.0/Resources/Schemas/SaveAuditV1.0.xsd" ::)

declare variable $traceabilityId as xs:string external;
declare variable $transactionId as xs:string external;
declare variable $system as xs:string external;
declare variable $requestDate as xs:dateTime external;
declare variable $responseDate as xs:dateTime external;
declare variable $eventType as xs:string external;
declare variable $message as element(*) external;
declare variable $service as xs:string? external;
declare variable $operation as xs:string external;
declare variable $user as xs:string external;

declare function local:IN_RegistroAuditoria($traceabilityId as xs:string, 
                                            $transactionId as xs:string, 
                                            $system as xs:string,
                                            $requestDate as xs:dateTime,
                                            $responseDate as xs:dateTime,
                                            $eventType as xs:string?,
                                            $message as element(*),
                                            $service as xs:string, 
                                            $operation as xs:string,
                                            $user as xs:string) 
                                            as element() (:: schema-element(ns1:saveAuditRequest) ::) {
    
   <ns1:saveAuditRequest>
       <ns1:item>
           <ns1:esbServerName></ns1:esbServerName>
           <ns1:traceabilityId>{fn:data($traceabilityId)}</ns1:traceabilityId>
           <ns1:transactionId>{fn:data($transactionId)}</ns1:transactionId>
           <ns1:system>{fn:data($system)}</ns1:system>
           <ns1:requestDate>{fn:data($requestDate)}</ns1:requestDate>
           <ns1:responseDate>{fn:data($responseDate)}</ns1:responseDate>
           <ns1:eventType>{fn:data($eventType)}</ns1:eventType>
           <ns1:message>{fn:concat("<![CDATA[",fn-bea:serialize($message),"]]>")}</ns1:message>
           <ns1:service>{fn:data($service)}</ns1:service>
           <ns1:operation>{fn:data($operation)}</ns1:operation>
           <ns1:user>{fn:data($user)}</ns1:user>
       </ns1:item>
   </ns1:saveAuditRequest>
};

local:IN_RegistroAuditoria($traceabilityId, $transactionId, $system, $requestDate, $responseDate,$eventType, $message, $service, $operation, $user)