xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $traceabilityId as xs:string external;
declare variable $body as element() external;
declare variable $fault as element() external;
declare variable $location as xs:string external;
declare variable $errorFlow as xs:string external;

declare function local:func($traceabilityId as xs:string, 
                            $body as element(), 
                            $fault as element(),
                            $location as xs:string,
                            $errorFlow as xs:string) 
                            as element() {
    <errorHandlingRequest>
      <traceabilityId>{fn:data($traceabilityId)}</traceabilityId>
      <body>{$body}</body>
      <fault>{$fault}</fault>
      <location>{fn:data($location)}</location>
    </errorHandlingRequest>
};

local:func($traceabilityId, $body, $fault,$location,$errorFlow)