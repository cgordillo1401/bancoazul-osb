xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $body as element() external;
declare variable $fault as element() external;



declare function local:getValueToFault($body as element(), 
                                       $fault as element()) 
                                       as xs:string 
{
    if ($fault//*:ValidationFailureDetail) then 
      "02"
    else
      if(local:contains-any-of(fn-bea:serialize($fault/*),("ConnectException","time","Time","404"))) then
      "03"
      else 
        if (exists($body//*:fault) or exists($body//*:Fault)) then
        "04"
        else 
          ("05")
};

declare function local:contains-any-of
  ( $arg as xs:string? ,
    $searchStrings as xs:string* )  as xs:boolean {

   some $searchString in $searchStrings
   satisfies contains($arg,$searchString)
 } ;

local:getValueToFault($body, $fault)