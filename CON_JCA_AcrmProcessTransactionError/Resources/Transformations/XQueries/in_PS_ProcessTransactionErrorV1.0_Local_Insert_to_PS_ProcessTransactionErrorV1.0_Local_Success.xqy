xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/T24ProcessTransactionError_Table";
(:: import schema at "../../JCA/T24ProcessTransactionError_Table.xsd" ::)


declare function local:func() as element() (:: schema-element(ns1:T24ProcessTransactionError_TableOutput) ::) {
    <ns1:T24ProcessTransactionError_TableOutput>
        <ns1:code>200</ns1:code>
        <ns1:message>Traza registrada exitosamente</ns1:message>
    </ns1:T24ProcessTransactionError_TableOutput>
};

local:func()